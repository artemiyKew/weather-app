import React, { Component } from 'react'
import { StyleSheet, View, Image } from 'react-native'
import { Text, Card, Divider } from 'react-native-elements'

export default class ForecastCard extends React.Component {
    render() {
        let time;

        let date = new Date(this.props.detail.dt * 1000)
        let day = date.getUTCDate()
        let month = date.getUTCMonth()
        let year = date.getFullYear()
        let h = date.getHours()

        let m = "0" + date.getMinutes()
        time = h + ':' + m
        return (
            <Card containerStyle={styles.card}>
               
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={styles.notes}>{this.props.location}</Text>
                    <Text style={styles.day}>{day}.{month + 1}.{year}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ width: 100, height: 100 }} source={{ uri: "https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png" }} />
                    <Text style={styles.time}>{time}</Text>
                </View>

                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical: 20 }} />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
                    <Text style={styles.notes}>{Math.round(this.props.detail.main.temp * 10) / 10}&#8451;</Text>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'rgba(56, 172, 236, 1)',
        borderWidth: 0,
        borderRadius: 20
    },
    time: {
        fontSize: 38,
        color: '#fff'
    },
    day: {
        fontSize: 18,
        color: '#fff'
    },
    notes: {
        fontSize: 18,
        color: '#fff',
        textTransform: 'capitalize'
    }
});