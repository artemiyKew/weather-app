import React, { Component } from 'react'
import { FlatList, ToastAndroid } from 'react-native'
import GPS from '@react-native-community/geolocation'
import ForecastCard from 'ForecastCard'
export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            latitude: 0,
            longitude: 0,
            forecast: []
        }
        GPS.getCurrentPosition(this.getPosition.bind(this), (e) => { ToastAndroid.show('Произошла ошибка получения данных с GPS.', ToastAndroid.LONG)},  { enableHighAccuracy: true, timeout: 20000});
    }
    getPosition(pos) {
        this.setState({
                latitude: pos.coords.latitude,
                longitude: pos.coords.longitude
            }, () => { this.getWeather(); }
        );
    }
    async getWeather() {
        let url = await fetch ('https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=298b00aec257aabbd5023093baeace57')
        console.log()
        let data = await url.json()
        console.log(data)
        this.setState({
            forecast: data
        })
    }

    render() {
        return (
            <FlatList data={this.state.forecast.list} style={{marginTop:20}} keyExtractor={item => item.dt_text} renderItem={({item}) => <ForecastCard detail={item} location={this.state.forecast.city.name} />} />
        );
    }
}